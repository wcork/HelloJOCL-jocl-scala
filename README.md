HelloJOCL-jocl
==============

This is a simple test of the jocl.org interface to OpenCL with scala.

A simple comparison between running on native scala serially, parallel collections and jocl is made.

In short, this progrm adds two randomly generated arrays and adds them together. The seeds are the same for each test so,
the values are identical for all of them.

### How to use:
build with
```
-> ./gradlew build
```
run with
```
-> ./gradlew run
```

Notes
-----
This is a re-implementation of the HelloWorld from jocl in Java.