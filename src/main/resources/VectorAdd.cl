#ifdef DOUBLE_FP
    #ifdef AMD_FP
        #pragma OPENCL EXTENSION cl_amd_fp64 : enable
    #else
        #pragma OPENCL EXTENSION cl_khr_fp64 : enable
    #endif
    typedef double varfloat;
#else
    typedef float varfloat;
#endif

// OpenCL Kernel Function for element by element vector addition
__kernel void
VectorAdd(__global const varfloat* a, __global const varfloat* b, __global varfloat* c, int numElements) {

    // get index into global data array
    int iGID = get_global_id(0);

    // bound check, equivalent to the limit on a 'for' loop
    if (iGID >= numElements)  {
        return;
    }

    // add the vector elements
    c[iGID] = a[iGID] + b[iGID];
}