package com.test.gpu

import scala.collection.parallel.immutable.ParRange
import scala.util.Random


class HelloScala {
  val size = 4194304

  lazy val arrayA = {
    Random.setSeed(12345)
    Array.fill(size)(Random.nextDouble * 100)
  }

  lazy val arrayB = {
    Random.setSeed(67890)
    Array.fill(size)(Random.nextDouble * 100)
  }

  lazy val arrayC = Array.ofDim[Double](size)

  def run(r: Int => Seq[Int]) {
    val seq = r(size)
    val t = for(n <- 1 to 10) yield {
      val startTime = System.nanoTime
      for (i <- seq) {
        arrayC(i) = arrayA(i) + arrayB(i)
      }
      val endTime = System.nanoTime

      System.gc()

      (endTime - startTime) / 1000
    }

    println("a+b=c results snapshot: ")
    for (i <- 0 until 10) {
      print(arrayC(i) + ", ")
    }
    println("...; " + (arrayC.length - 10) + " more")

//    t map ("computation took: " + _ + " micro sec") foreach println

    for ( i <- t.indices ) {
      println("computation took %2d: %d µs" format(i+1, t(i)))
    }
    println()
    println("Total time         : %d µs" format t.sum)
    println("Average time       : %d µs" format(t.sum / t.length))
  }
  def parRun(r: Int => ParRange) {
    val seq = r(size)
    val t = for(n <- 1 to 10) yield {
      val startTime = System.nanoTime
      for (i <- seq.par) {
        arrayC(i) = arrayA(i) + arrayB(i)
      }
      val endTime = System.nanoTime

      System.gc()

      (endTime - startTime) / 1000
    }

    println("a+b=c results snapshot: ")
    for (i <- 0 until 10) {
      print(arrayC(i) + ", ")
    }
    println("...; " + (arrayC.length - 10) + " more")

//    t map ("computation took: " + _ + " micro sec") foreach println

    for ( i <- t.indices ) {
      println("computation took %2d: %d µs" format(i+1, t(i)))
    }
    println()
    println("Total time         : %d µs" format t.sum)
    println("Average time       : %d µs" format(t.sum / t.length))
  }
}

object HelloScala {
  def main(args: Array[String]) {
    println("==== Linear Scala ====")
    new HelloScala().run(0 until _)
    println("==== Parallel Scala ==== - availableProcessors: " + scala.collection.parallel.availableProcessors)
    new HelloScala().parRun(0 until _ par)
  }
}