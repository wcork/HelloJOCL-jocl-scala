/*
 * JOCL - Java bindings for OpenCL
 *
 * Copyright 2010 Marco Hutter - http://www.jocl.org/
 *
 * Translated to Scala by Piotr Tarsa (http://github.com/tarsa)
 */

package com.test.gpu

import java.nio._

import org.jocl.CL._
import org.jocl._

/**
 * A JOCL program that queries and prints information about all
 * available devices.
 */
object JOCLDeviceQuery {
  /**
   * The entry point of this program
   *
   * @param args Not used
   */
  def main(args: Array[String]): Unit = {
    val platforms = {
      // Obtain the number of platforms
      val numPlatforms = Array(0)
      clGetPlatformIDs(0, null, numPlatforms)

      println(s"Number of platforms: ${numPlatforms(0)}")

      // Obtain the platform IDs
      val platformsArray = Array.ofDim[cl_platform_id](numPlatforms(0))
      clGetPlatformIDs(platformsArray.length, platformsArray, null)
      platformsArray
    }

    // Collect all devices of all platforms
    val devices = platforms.flatMap { platform =>
      val platformName = getString(platform, CL_PLATFORM_NAME)

      // Obtain the number of devices for the current platform
      val numDevices = Array(0)
      clGetDeviceIDs(platform, CL_DEVICE_TYPE_ALL, 0, null, numDevices)

      println(s"Number of devices in platform $platformName: ${numDevices(0)}")

      val devicesArray = Array.ofDim[cl_device_id](numDevices(0))
      clGetDeviceIDs(platform, CL_DEVICE_TYPE_ALL, numDevices(0), devicesArray,
        null)

      devicesArray
    }

    // Print the infos about all devices
    for (device <- devices) {
      // CL_DEVICE_NAME
      val deviceName = getString(device, CL_DEVICE_NAME)
      println(s"--- Info for device $deviceName: ---")
      println(s"CL_DEVICE_NAME: \t\t\t$deviceName")

      // CL_DEVICE_VENDOR
      val deviceVendor = getString(device, CL_DEVICE_VENDOR)
      println(s"CL_DEVICE_VENDOR: \t\t\t$deviceVendor")

      // CL_DRIVER_VERSION
      val driverVersion = getString(device, CL_DRIVER_VERSION)
      println(s"CL_DRIVER_VERSION: \t\t\t$driverVersion")

      // CL_DEVICE_OPENCL_C_VERSION
      val openclVersion = getString(device, CL_DEVICE_OPENCL_C_VERSION)
      println(s"CL_DEVICE_OPENCL_C_VERSION: \t\t$openclVersion")

      // CL_DEVICE_TYPE
      val deviceType = getLong(device, CL_DEVICE_TYPE)
      if( (deviceType & CL_DEVICE_TYPE_CPU) != 0)
        println("CL_DEVICE_TYPE:\t\t\t\tCL_DEVICE_TYPE_CPU")
      if( (deviceType & CL_DEVICE_TYPE_GPU) != 0)
        println("CL_DEVICE_TYPE:\t\t\t\tCL_DEVICE_TYPE_GPU")
      if( (deviceType & CL_DEVICE_TYPE_ACCELERATOR) != 0)
        println("CL_DEVICE_TYPE:\t\t\t\tCL_DEVICE_TYPE_ACCELERATOR")
      if( (deviceType & CL_DEVICE_TYPE_DEFAULT) != 0)
        println("CL_DEVICE_TYPE:\t\t\t\tCL_DEVICE_TYPE_DEFAULT")

      // CL_DEVICE_MAX_COMPUTE_UNITS
      val maxComputeUnits = getInt(device, CL_DEVICE_MAX_COMPUTE_UNITS)
      println(s"CL_DEVICE_MAX_COMPUTE_UNITS:\t\t$maxComputeUnits")

      // CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS
      val maxWorkItemDimensions = getLong(device,
        CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS)
      println(s"CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS:\t$maxWorkItemDimensions")

      // CL_DEVICE_MAX_WORK_ITEM_SIZES
      val maxWorkItemSizes = getSizes(device, CL_DEVICE_MAX_WORK_ITEM_SIZES, 3)
      println(s"CL_DEVICE_MAX_WORK_ITEM_SIZES:\t\t${maxWorkItemSizes(0)} / " +
        s"${maxWorkItemSizes(1)} / ${maxWorkItemSizes(2)} ")

      // CL_DEVICE_MAX_WORK_GROUP_SIZE
      val maxWorkGroupSize = getSize(device, CL_DEVICE_MAX_WORK_GROUP_SIZE)
      println(s"CL_DEVICE_MAX_WORK_GROUP_SIZE:\t\t$maxWorkGroupSize")

      // CL_DEVICE_MAX_CLOCK_FREQUENCY
      val maxClockFrequency = getLong(device, CL_DEVICE_MAX_CLOCK_FREQUENCY)
      println(s"CL_DEVICE_MAX_CLOCK_FREQUENCY:\t\t$maxClockFrequency MHz")

      // CL_DEVICE_ADDRESS_BITS
      val addressBits = getInt(device, CL_DEVICE_ADDRESS_BITS)
      println(s"CL_DEVICE_ADDRESS_BITS:\t\t\t$addressBits")

      // CL_DEVICE_MAX_MEM_ALLOC_SIZE
      val maxMemAllocSize = getLong(device, CL_DEVICE_MAX_MEM_ALLOC_SIZE)
      println(s"CL_DEVICE_MAX_MEM_ALLOC_SIZE:\t\t${maxMemAllocSize /
        (1024 * 1024)} MByte")

      // CL_DEVICE_GLOBAL_MEM_SIZE
      val globalMemSize = getLong(device, CL_DEVICE_GLOBAL_MEM_SIZE)
      println(s"CL_DEVICE_GLOBAL_MEM_SIZE:\t\t${globalMemSize /
        (1024 * 1024)} MByte")

      // CL_DEVICE_ERROR_CORRECTION_SUPPORT
      val errorCorrectionSupport = getInt(device,
        CL_DEVICE_ERROR_CORRECTION_SUPPORT)
      println(s"CL_DEVICE_ERROR_CORRECTION_SUPPORT:\t${
        if(errorCorrectionSupport != 0) "yes" else "no"}")

      // CL_DEVICE_LOCAL_MEM_TYPE
      val localMemType = getInt(device, CL_DEVICE_LOCAL_MEM_TYPE)
      println(s"CL_DEVICE_LOCAL_MEM_TYPE:\t\t${
        if(localMemType == 1) "local" else "global"}")

      // CL_DEVICE_LOCAL_MEM_SIZE
      val localMemSize = getLong(device, CL_DEVICE_LOCAL_MEM_SIZE)
      println(s"CL_DEVICE_LOCAL_MEM_SIZE:\t\t${localMemSize / 1024} KByte")

      // CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE
      val maxConstantBufferSize = getLong(device,
        CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE)
      println(s"CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE:\t${maxConstantBufferSize /
        1024} KByte")

      // CL_DEVICE_QUEUE_PROPERTIES
      val queueProperties = getLong(device, CL_DEVICE_QUEUE_PROPERTIES)
      if(( queueProperties & CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE ) != 0)
        println("CL_DEVICE_QUEUE_PROPERTIES:\t\t" +
          "CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE")
      if(( queueProperties & CL_QUEUE_PROFILING_ENABLE ) != 0)
        println("CL_DEVICE_QUEUE_PROPERTIES:\t\t" +
          "CL_QUEUE_PROFILING_ENABLE")

      // CL_DEVICE_IMAGE_SUPPORT
      val imageSupport = getInt(device, CL_DEVICE_IMAGE_SUPPORT)
      println(s"CL_DEVICE_IMAGE_SUPPORT:\t\t$imageSupport")

      // CL_DEVICE_MAX_READ_IMAGE_ARGS
      val maxReadImageArgs = getInt(device, CL_DEVICE_MAX_READ_IMAGE_ARGS)
      println(s"CL_DEVICE_MAX_READ_IMAGE_ARGS:\t\t$maxReadImageArgs")

      // CL_DEVICE_MAX_WRITE_IMAGE_ARGS
      val maxWriteImageArgs = getInt(device, CL_DEVICE_MAX_WRITE_IMAGE_ARGS)
      println(s"CL_DEVICE_MAX_WRITE_IMAGE_ARGS:\t\t$maxWriteImageArgs")

      // CL_DEVICE_SINGLE_FP_CONFIG
      val singleFpConfig = getLong(device, CL_DEVICE_SINGLE_FP_CONFIG)
      println(s"CL_DEVICE_SINGLE_FP_CONFIG:\t\t${
        stringFor_cl_device_fp_config(singleFpConfig)}")

      // CL_DEVICE_DOUBLE_FP_CONFIG
      val doubleFpConfig = getLong(device, CL_DEVICE_DOUBLE_FP_CONFIG)
      println(s"CL_DEVICE_DOUBLE_FP_CONFIG:\t\t${
        stringFor_cl_device_fp_config(doubleFpConfig)}")

      // CL_DEVICE_IMAGE2D_MAX_WIDTH
      val image2dMaxWidth = getSize(device, CL_DEVICE_IMAGE2D_MAX_WIDTH)
      println(s"CL_DEVICE_2D_MAX_WIDTH\t\t\t$image2dMaxWidth")

      // CL_DEVICE_IMAGE2D_MAX_HEIGHT
      val image2dMaxHeight = getSize(device, CL_DEVICE_IMAGE2D_MAX_HEIGHT)
      println(s"CL_DEVICE_2D_MAX_HEIGHT\t\t\t$image2dMaxHeight")

      // CL_DEVICE_IMAGE3D_MAX_WIDTH
      val image3dMaxWidth = getSize(device, CL_DEVICE_IMAGE3D_MAX_WIDTH)
      println(s"CL_DEVICE_3D_MAX_WIDTH\t\t\t$image3dMaxWidth")

      // CL_DEVICE_IMAGE3D_MAX_HEIGHT
      val image3dMaxHeight = getSize(device, CL_DEVICE_IMAGE3D_MAX_HEIGHT)
      println(s"CL_DEVICE_3D_MAX_HEIGHT\t\t\t$image3dMaxHeight")

      // CL_DEVICE_IMAGE3D_MAX_DEPTH
      val image3dMaxDepth = getSize(device, CL_DEVICE_IMAGE3D_MAX_DEPTH)
      println(s"CL_DEVICE_3D_MAX_DEPTH\t\t\t$image3dMaxDepth")

      // CL_DEVICE_PREFERRED_VECTOR_WIDTH_<type>
      print("CL_DEVICE_PREFERRED_VECTOR_WIDTH_<t>\t")
      val preferredVectorWidthChar = getInt(device,
        CL_DEVICE_PREFERRED_VECTOR_WIDTH_CHAR)
      val preferredVectorWidthShort = getInt(device,
        CL_DEVICE_PREFERRED_VECTOR_WIDTH_SHORT)
      val preferredVectorWidthInt = getInt(device,
        CL_DEVICE_PREFERRED_VECTOR_WIDTH_INT)
      val preferredVectorWidthLong = getInt(device,
        CL_DEVICE_PREFERRED_VECTOR_WIDTH_LONG)
      val preferredVectorWidthFloat = getInt(device,
        CL_DEVICE_PREFERRED_VECTOR_WIDTH_FLOAT)
      val preferredVectorWidthDouble = getInt(device,
        CL_DEVICE_PREFERRED_VECTOR_WIDTH_DOUBLE)
      println(s"CHAR $preferredVectorWidthChar, " +
        s"SHORT $preferredVectorWidthShort, " +
        s"INT $preferredVectorWidthInt, " +
        s"LONG $preferredVectorWidthLong, " +
        s"FLOAT $preferredVectorWidthFloat, " +
        s"DOUBLE $preferredVectorWidthDouble")

      // CL_DEVICE_EXTENSIONS
      val deviceExtensions = getString(device, CL_DEVICE_EXTENSIONS)
      println(s"CL_DEVICE_EXTENSIONS\t\t\t$deviceExtensions")

      print("\n\n")
    }
  }

  /**
   * Returns the value of the device info parameter with the given name
   *
   * @param device The device
   * @param paramName The parameter name
   * @return The value
   */
  private def getInt(device: cl_device_id, paramName: Int): Int = {
    getInts(device, paramName, 1)(0)
  }

  /**
   * Returns the values of the device info parameter with the given name
   *
   * @param device The device
   * @param paramName The parameter name
   * @param numValues The number of values
   * @return The value
   */
  private def getInts(device: cl_device_id, paramName: Int,
                      numValues: Int): Array[Int] = {
    val values = Array.ofDim[Int](numValues)
    clGetDeviceInfo(device, paramName, Sizeof.cl_int * numValues,
      Pointer.to(values), null)
    values
  }

  /**
   * Returns the value of the device info parameter with the given name
   *
   * @param device The device
   * @param paramName The parameter name
   * @return The value
   */
  private def getLong(device: cl_device_id, paramName: Int): Long = {
    getLongs(device, paramName, 1)(0)
  }

  /**
   * Returns the values of the device info parameter with the given name
   *
   * @param device The device
   * @param paramName The parameter name
   * @param numValues The number of values
   * @return The value
   */
  private def getLongs(device: cl_device_id, paramName: Int,
                       numValues: Int): Array[Long]= {
    val values = Array.ofDim[Long](numValues)
    clGetDeviceInfo(device, paramName, Sizeof.cl_long * numValues,
      Pointer.to(values), null)
    values
  }

  /**
   * Returns the value of the device info parameter with the given name
   *
   * @param device The device
   * @param paramName The parameter name
   * @return The value
   */
  private def getString(device: cl_device_id, paramName: Int): String = {
    // Obtain the length of the string that will be queried
    val size = Array(0L)
    clGetDeviceInfo(device, paramName, 0, null, size)

    // Create a buffer of the appropriate size and fill it with the info
    val buffer = Array.ofDim[Byte](size(0).toInt)
    clGetDeviceInfo(device, paramName, buffer.length, Pointer.to(buffer), null)

    // Create a string from the buffer (excluding the trailing \0 byte)
    new String(buffer, 0, buffer.length - 1)
  }

  /**
   * Returns the value of the platform info parameter with the given name
   *
   * @param platform The platform
   * @param paramName The parameter name
   * @return The value
   */
  private def getString(platform: cl_platform_id, paramName: Int): String = {
    // Obtain the length of the string that will be queried
    val size = Array(0L)
    clGetPlatformInfo(platform, paramName, 0, null, size)

    // Create a buffer of the appropriate size and fill it with the info
    val buffer = Array.ofDim[Byte](size(0).toInt)
    clGetPlatformInfo(platform, paramName, buffer.length, Pointer.to(buffer),
      null)

    // Create a string from the buffer (excluding the trailing \0 byte)
    new String(buffer, 0, buffer.length - 1)
  }

  /**
   * Returns the value of the device info parameter with the given name
   *
   * @param device The device
   * @param paramName The parameter name
   * @return The value
   */
  private def getSize(device: cl_device_id, paramName: Int): Long = {
    getSizes(device, paramName, 1)(0)
  }

  /**
   * Returns the values of the device info parameter with the given name
   *
   * @param device The device
   * @param paramName The parameter name
   * @param numValues The number of values
   * @return The value
   */
  def getSizes(device: cl_device_id, paramName: Int,
               numValues: Int): Array[Long] = {
    // The size of the returned data has to depend on
    // the size of a size_t, which is handled here
    val buffer = ByteBuffer.allocate(numValues * Sizeof.size_t).order(
      ByteOrder.nativeOrder())
    clGetDeviceInfo(device, paramName, Sizeof.size_t * numValues,
      Pointer.to(buffer), null)
    val values = Array.ofDim[Long](numValues)
    for (i <- values.indices) {
      values(i) = if (Sizeof.size_t == 4) {
        buffer.getInt(i * Sizeof.size_t)
      } else {
        buffer.getLong(i * Sizeof.size_t)
      }
    }
    values
  }
}