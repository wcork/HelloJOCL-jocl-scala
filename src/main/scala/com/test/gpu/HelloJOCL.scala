package com.test.gpu

import java.nio.{ByteBuffer, ByteOrder}

import org.jocl.CL._
import org.jocl._

import scala.util.Random

class HelloJOCL(args: Array[String]) {

  // The platform, device type and device number
  // that will be used
  final val platformIndex = 0
  final val prefDeviceType = CL_DEVICE_TYPE_GPU
  final val deviceIndex = 0

  // Length of arrays to process
  final val elementCount = 4194304L

  // Enable exceptions and subsequently omit error checks in this sample
  CL.setExceptionsEnabled(true)

  // Obtain the number of platforms
  var numPlatformsArray = new Array[Int](1)
  clGetPlatformIDs(0, null, numPlatformsArray) //TODO: Find why this takes so long on ncxt-gpu
  var numPlatforms = numPlatformsArray(0)

  println(s"Number of platforms: $numPlatforms")

  // Obtain a platform ID
  val platforms = {
    // Obtain the platform IDs
    val platformsArray = Array.ofDim[cl_platform_id](numPlatforms)
    clGetPlatformIDs(platformsArray.length, platformsArray, null)
    platformsArray
  }

  // Obtain the number of devices for the platform
  //  var numDevicesArray = new Array[Int](1)
  //  clGetDeviceIDs(platform, deviceType, 0, null, numDevicesArray)
  //  val numDevices = numDevicesArray(0)

  // Obtain a device ID
  // Collect all devices of all platforms
  val devices = platforms.flatMap { platform =>
    // Obtain the number of devices for the current platform
    val numDevices = Array(0)
    clGetDeviceIDs(platform, CL_DEVICE_TYPE_ALL, 0, null, numDevices)
    val devicesArray = Array.ofDim[cl_device_id](numDevices(0))
    clGetDeviceIDs(platform, CL_DEVICE_TYPE_ALL, numDevices(0), devicesArray, null)

    devicesArray
  }

  //Look for the first GPU, if there isn't one, use first device.
  val device = {
    if (devices.length < 1) throw new Exception("No OpenCL device found")
    if (args.length > 0) {
      if (args(0).toInt > devices.length) throw new Exception("No OpenCL device found at index " + args(0).toInt)
      devices(args(0).toInt)
    } else {
      val devCpus = devices.filter(getLong(_, CL_DEVICE_TYPE) == prefDeviceType) // get GPUs
      if (devCpus.length > 0) {
        devCpus(0)
      } else {
        devices(0) // If no GPU, use first OpenCL Device.
      }
    }
  }

  println("--- Devices ---")
  var devInd = 0
  for (dev <- devices) {
    val deviceName = getString(dev, CL_DEVICE_NAME).trim
    val deviceVendor = getString(dev, CL_DEVICE_VENDOR)
    val deviceType = getDeviceTypeString(dev)
    val deviceFreq = getLong(dev, CL_DEVICE_MAX_CLOCK_FREQUENCY)
    if (dev == device) print(s"[$devInd] ") else print(s" $devInd  ")
    val deviceBits = if (getString(dev, CL_DEVICE_EXTENSIONS).contains("cl_khr_fp64")) "64-bit" else "32-bit"
    println(s"$deviceName - $deviceFreq Mhz - ($deviceVendor) -- $deviceType  |  $deviceBits")
    devInd += 1
  }

  //TODO: Determine platform from device so properties work
  var platform = platforms(platformIndex)
  // Initialize the context properties
  var contextProperties = null //new cl_context_properties()
  //  contextProperties.addProperty(CL_CONTEXT_PLATFORM, platform)

  // Create a context for the selected device
  val context = clCreateContext(contextProperties, 1, Array(device), null, null, null)

  //Test if 64-bit
  val is64Bit = getString(device, CL_DEVICE_EXTENSIONS).contains("cl_khr_fp64")

  // Create a command-queue, with profiling info enabled
  var properties = 0L
//  properties = properties | CL.CL_QUEUE_PROFILING_ENABLE
  val commandQueue = clCreateCommandQueue(context, device, properties, null)

  // Allocate the memory objects for the input- and output data
  val clBufferA = {
    Random.setSeed(12345)
    if (is64Bit)
      Array.fill(elementCount.asInstanceOf[Int])(Random.nextDouble * 100)
    else
      Array.fill(elementCount.asInstanceOf[Int])(Random.nextFloat * 100)
  }
  val clBufferB = {
    Random.setSeed(67890)
    if (is64Bit)
      Array.fill(elementCount.asInstanceOf[Int])(Random.nextDouble * 100)
    else
      Array.fill(elementCount.asInstanceOf[Int])(Random.nextFloat * 100)
  }
  val clBufferC = {
    if (is64Bit)
      new Array[Double](elementCount.asInstanceOf[Int])
    else
      new Array[Float](elementCount.asInstanceOf[Int])
  }

  val srcA = {
    if (is64Bit) Pointer.to(clBufferA.asInstanceOf[Array[Double]])
    else Pointer.to(clBufferA.asInstanceOf[Array[Float]])
  }
  val srcB = {
    if (is64Bit) Pointer.to(clBufferB.asInstanceOf[Array[Double]])
    else Pointer.to(clBufferB.asInstanceOf[Array[Float]])
  }
  val dst = {
    if (is64Bit) Pointer.to(clBufferC.asInstanceOf[Array[Double]])
    else Pointer.to(clBufferC.asInstanceOf[Array[Float]])
  }

  var elementSizes = {
    if (is64Bit) Sizeof.cl_double else Sizeof.cl_float
  }
  var memObjects = new Array[cl_mem](3)
  memObjects(0) = clCreateBuffer(context,
    CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
    elementSizes * elementCount, srcA, null)
  memObjects(1) = clCreateBuffer(context,
    CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
    elementSizes * elementCount, srcB, null)
  memObjects(2) = clCreateBuffer(context,
    CL_MEM_WRITE_ONLY,
    elementSizes * elementCount, null, null)

  // Create the program from the source code
  val programSource = Array[String](scala.io.Source.fromInputStream(classOf[HelloJOCL].getResourceAsStream("/VectorAdd.cl")).mkString)
  val program = clCreateProgramWithSource(context, 1, programSource, null, null)

  // Build the program
  var buildParams = if (is64Bit) "-D DOUBLE_FP" else ""
  clBuildProgram(program, 0, null, buildParams, null, null)

  // Set the work-item dimensions
  val local_work_size = Array(getSize(device, CL_DEVICE_MAX_WORK_GROUP_SIZE))
  val global_work_size = Array(roundUp(local_work_size(0), elementCount))

  // Create the kernel
  var kernel: cl_kernel = {
    println("used device memory: " +
      (clBufferA.length * elementSizes + clBufferB.length * elementSizes + clBufferC.length * elementSizes) / 1024 / 1024 + " MiB")
    println("localWorkSize: " + local_work_size(0) + ", globalWorkSize: " + global_work_size(0))

    clCreateKernel(program, "VectorAdd", null)
  }

  // Set the arguments for the kernel
  clSetKernelArg(kernel, 0, Sizeof.cl_mem, Pointer.to(memObjects(0)))
  clSetKernelArg(kernel, 1, Sizeof.cl_mem, Pointer.to(memObjects(1)))
  clSetKernelArg(kernel, 2, Sizeof.cl_mem, Pointer.to(memObjects(2)))
  clSetKernelArg(kernel, 3, Sizeof.cl_int, Pointer.to(Array[Int](elementCount.asInstanceOf[Int])))


  def roundUp(groupSize: Long, globalSize: Long) = {
    val r = globalSize % groupSize
    if (r == 0) globalSize
    else globalSize + groupSize - r
  }

  def getDeviceTypeString(device: cl_device_id): String = {
    // CL_DEVICE_TYPE
    val deviceType = getLong(device, CL_DEVICE_TYPE)
    deviceType match {
      case CL_DEVICE_TYPE_CPU => "CL_DEVICE_TYPE_CPU"
      case CL_DEVICE_TYPE_GPU => "CL_DEVICE_TYPE_GPU"
      case CL_DEVICE_TYPE_ACCELERATOR => "CL_DEVICE_TYPE_ACCELERATOR"
      case CL_DEVICE_TYPE_DEFAULT => "CL_DEVICE_TYPE_DEFAULT"
    }
  }

  /**
   * Returns the value of the device info parameter with the given name
   *
   * @param device The device
   * @param paramName The parameter name
   * @return The value
   */
  private def getSize(device: cl_device_id, paramName: Int): Long = {
    getSizes(device, paramName, 1)(0)
  }

  /**
   * Returns the values of the device info parameter with the given name
   *
   * @param device The device
   * @param paramName The parameter name
   * @param numValues The number of values
   * @return The value
   */
  private def getSizes(device: cl_device_id, paramName: Int,
                       numValues: Int): Array[Long] = {
    // The size of the returned data has to depend on
    // the size of a size_t, which is handled here
    val buffer = ByteBuffer.allocate(numValues * Sizeof.size_t).order(
      ByteOrder.nativeOrder())
    clGetDeviceInfo(device, paramName, Sizeof.size_t * numValues,
      Pointer.to(buffer), null)
    val values = Array.ofDim[Long](numValues)
    for (i <- values.indices) {
      values(i) = if (Sizeof.size_t == 4) {
        buffer.getInt(i * Sizeof.size_t)
      } else {
        buffer.getLong(i * Sizeof.size_t)
      }
    }
    values
  }

  /**
   * Returns the value of the device info parameter with the given name
   *
   * @param device The device
   * @param paramName The parameter name
   * @return The value
   */
  private def getInt(device: cl_device_id, paramName: Int): Int = {
    getInts(device, paramName, 1)(0)
  }

  /**
   * Returns the values of the device info parameter with the given name
   *
   * @param device The device
   * @param paramName The parameter name
   * @param numValues The number of values
   * @return The value
   */
  private def getInts(device: cl_device_id, paramName: Int,
                      numValues: Int): Array[Int] = {
    val values = Array.ofDim[Int](numValues)
    clGetDeviceInfo(device, paramName, Sizeof.cl_int * numValues,
      Pointer.to(values), null)
    values
  }

  /**
   * Returns the value of the device info parameter with the given name
   *
   * @param device The device
   * @param paramName The parameter name
   * @return The value
   */
  private def getLong(device: cl_device_id, paramName: Int): Long = {
    getLongs(device, paramName, 1)(0)
  }

  /**
   * Returns the values of the device info parameter with the given name
   *
   * @param device The device
   * @param paramName The parameter name
   * @param numValues The number of values
   * @return The value
   */
  private def getLongs(device: cl_device_id, paramName: Int,
                       numValues: Int): Array[Long] = {
    val values = Array.ofDim[Long](numValues)
    clGetDeviceInfo(device, paramName, Sizeof.cl_long * numValues,
      Pointer.to(values), null)
    values
  }

  /**
   * Returns the value of the device info parameter with the given name
   *
   * @param device The device
   * @param paramName The parameter name
   * @return The value
   */
  private def getString(device: cl_device_id, paramName: Int): String = {
    // Obtain the length of the string that will be queried
    val size = Array(0L)
    clGetDeviceInfo(device, paramName, 0, null, size)

    // Create a buffer of the appropriate size and fill it with the info
    val buffer = Array.ofDim[Byte](size(0).toInt)
    clGetDeviceInfo(device, paramName, buffer.length, Pointer.to(buffer), null)

    // Create a string from the buffer (excluding the trailing \0 byte)
    new String(buffer, 0, buffer.length - 1)
  }

  /**
   * Returns the value of the platform info parameter with the given name
   *
   * @param platform The platform
   * @param paramName The parameter name
   * @return The value
   */
  private def getString(platform: cl_platform_id, paramName: Int): String = {
    // Obtain the length of the string that will be queried
    val size = Array(0L)
    clGetPlatformInfo(platform, paramName, 0, null, size)

    // Create a buffer of the appropriate size and fill it with the info
    val buffer = Array.ofDim[Byte](size(0).toInt)
    clGetPlatformInfo(platform, paramName, buffer.length, Pointer.to(buffer),
      null)

    // Create a string from the buffer (excluding the trailing \0 byte)
    new String(buffer, 0, buffer.length - 1)
  }

  def run() = {

    // Execute the kernel
    val event = new cl_event()
    clEnqueueNDRangeKernel(commandQueue, kernel, 1, null, global_work_size, local_work_size, 0, null, event)

    // Read the output data
    clEnqueueReadBuffer(commandQueue, memObjects(2), CL_TRUE, 0, elementCount * Sizeof.cl_float, dst, 0, null, null)

//    printf("Kernel event:")
//    val queueTime = Array.ofDim[Long](1)
//    val submitTime = Array.ofDim[Long](1)
//    val startTime = Array.ofDim[Long](1)
//    val endTime = Array.ofDim[Long](1)
//    clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_QUEUED, Sizeof.cl_ulong, Pointer.to(queueTime), null)
//    clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_SUBMIT, Sizeof.cl_ulong, Pointer.to(submitTime), null)
//    clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_START, Sizeof.cl_ulong, Pointer.to(startTime), null)
//    clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_END, Sizeof.cl_ulong, Pointer.to(endTime), null)
//    println("\tQUEUED Time: " + queueTime(0))
//    println("\tSUBMIT Time: +" + (submitTime(0) - queueTime(0)) / 1e3 + " µs")
//    println("\tSTART Time: +" + (startTime(0) - queueTime(0)) / 1e3 + " µs")
//    println("\tEND Time: +" + (endTime(0) - queueTime(0)) / 1e3 + " µs")

    clBufferC
  }

  def release() = {
    // Release kernel, program, and memory objects
    clReleaseMemObject(memObjects(0))
    clReleaseMemObject(memObjects(1))
    clReleaseMemObject(memObjects(2))
    clReleaseKernel(kernel)
    clReleaseProgram(program)
    clReleaseCommandQueue(commandQueue)
    clReleaseContext(context)
  }

}

object HelloJOCL {

  def main(args: Array[String]) {
    val bsCall = System.nanoTime
    println("==== JOCL ====")

    val hello = new HelloJOCL(args)
    var ret: Array[_ >: Double with Float] = null
    val t = for (i <- 1 to 10) yield {
      val startTime = System.nanoTime
      ret = hello.run()
      val endTime = System.nanoTime

      val time = (endTime - startTime) / 1000
      // print first few elements of the resulting buffer to the console.
      if (i == 1) {
        println("a+b=c results snapshot: ")
        for (i <- 0 until 10) {
          print(ret(i) + ", ")
        }
        println("...; " + (ret.length - 10) + " more")
      }
      println("computation took %2d: %d µs" format(i, time))

      time
    }
    println()
    println("Total time         : %d µs" format t.sum)
    println("Average time       : %d µs" format(t.sum / t.length))
    hello.release()
  }
}